/**
 * Copyright (c) 2015 Voidware Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS," WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 * 
 * contact@voidware.com
 */


#include <stdio.h>
#include <ctype.h>
#include <iostream>
#include <windows.h>

#define MAX_LINE        256

static char line[MAX_LINE];
int debug = 0;

const char* getline(FILE* fp)
{
    int c;
    char* p = line;
    while ((c = getc(fp)) != EOF)
    {
        if (c == '\n') break;
        if (p - line < MAX_LINE-1) *p++ = c;
    }
    *p = 0;
    
    if (c == EOF && !*line) return 0;
    return line;
}


inline bool options(int argc, char** argv)
{
    int i;
    bool res = true;
    for (i = 1; i < argc; ++i)
    {
        if (argv[i][0] == '-')
        {
            if (!strncmp(argv[i], "-d", 2)) 
            {
                int l = atoi(argv[i] + 2);
                if (!l) l = 1;
                debug = l;
            }
            else
            {
                std::cout << "Unrecognised option '" << argv[i] << "'\n";
                res = false;
            }
        }
    }
    return res;
}

using namespace std;

bool processFile(FILE* header, const char* name)
{
    cout << "processing " << name << endl;
    
    char* bak = new char[strlen(name) + 2];
    strcat(strcpy(bak, name), "~");
    
    if (debug) cout << "delete " << bak << endl;
    DeleteFile(bak);

    if (debug) cout << "move " << name << " -> " << bak << endl;
    if (!MoveFile(name, bak)) return false;

    FILE* fp = fopen(name, "w");
    if (!fp) 
    {
        cout << "cant open output file '" << name << "'\n";
        return false;
    }

    // copy the new header
    fseek(header, 0, SEEK_SET);
    const char* s;
    while ((s = getline(header)) != 0)
    {
        fputs(s, fp);
        fputc('\n', fp);
    }

    // copy the original skipping the header
    FILE* fin = fopen(bak, "r");
    if (!fin)
    {
        cout << "cant open input file '" << bak << "'\n";
        fclose(fp);
        return false;
    }

    // skip the first header of the original (if exists)
    int c;
    bool slash = false;
    bool star = false;
    bool comment = false;
    while ((c = getc(fin)) != EOF)
    {
        if (!comment)
        {
            if (isspace(c)) continue;
            if (c == '*' && slash) comment = true;
            else 
            {
                slash = (c == '/');
                if (!slash)
                {
                    // does not start with comment.
                    ungetc(c, fin);
                    break;
                }
            }
        }
        else
        {
            if (c == '/' && star) 
            {
                comment = false;
                break;
            }
            star = (c == '*');
        }
    }

    bool begin = true;

    // emit remainder of file.
    while ((c = getc(fin)) != EOF)
    {
        if (!isspace(c))
        {
            if (begin) 
            {
                fputc('\n', fp); // emit one blank line after header
                begin = false;
            }
        }

        if (!begin) fputc(c, fp);
    }
    
    fclose(fp);

    delete bak;
    return true;
}

int main(int argc, char** argv)
{
    bool ok = options(argc, argv);

    int i;
    bool opt = false;
    FILE* header = 0;
    int cc = 0;

    if (ok) for (i = 1; i < argc; ++i)
    {
        if (argv[i][0] != '-')
        {
            if (!header)
            {
                header = fopen(argv[i], "r");
                if (!header)
                {
                    cout << "Cant open header '" << argv[i] << "'\n";
                    return -1;
                }
            }
            else
            {
                if (!processFile(header, argv[i]))
                    cout << "Failed to process " << argv[i] << endl;
                
                ++cc;
            }
        }
    }

    if (header) fclose(header);

    if (!ok || !cc)
    {
        cout << "Usage: " << argv[0] << " new-header-file [file...]\n";
        return -1;
    }

    return 0;
}
